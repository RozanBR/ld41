﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour {

	// Use this for initialization
	void Start ( )
    {
        StartCoroutine ( LookAt ( ) );
		
	}

    private IEnumerator LookAt ( )
    {
        do
        {

            transform.LookAt ( Camera.main.GetComponent<Transform> ( ) );
            transform.localRotation = Quaternion.Euler ( transform.rotation.eulerAngles.x, 
                                                    transform.rotation.eulerAngles.y - 180.0f,
                                                    transform.rotation.eulerAngles.z );
            yield return null;

        } while ( true );
    }
}
