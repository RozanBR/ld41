﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid {
	public Vector3 position;

	public Grid(Vector3 pos) 
	{
		this.position = pos;
	}
}

public class EnemyManager : MonoBehaviour {

	List<Enemy> Enemies = new List<Enemy>();

    Vector2 min_boundary;
    Vector2 max_boundary;

    List<Grid> []grid = null;

	public List<GameObject> instanceEnemy;
	public float depth = 0.0f;

    public int numberOfEnemies = 3;
    public float gridSpacing = 1.5f;
    public float desiredZPositionForEnemies = 50.0f;
 
	// Use this for initialization
	void Start () 
	{
        min_boundary = Camera.main.ScreenToWorldPoint ( new Vector3 ( Camera.main.pixelWidth * 0.1f, Camera.main.pixelHeight * 0.4f, desiredZPositionForEnemies ) );
        max_boundary = Camera.main.ScreenToWorldPoint ( new Vector3 ( Camera.main.pixelWidth * 0.90f, Camera.main.pixelHeight * 0.90f, desiredZPositionForEnemies ) );

	}
	
	// Update is called once per frame
	void Update () 
	{
        //if (Input.GetKeyDown(KeyCode.H))
        //	Respawn(numberOfEnemies, gridSpacing);

    }
    
    public void Respawn ( int num_enemies, float gridSize = -1.0f ) 
	{
        gridSize = gridSpacing;
		createGrid( gridSize );

		cleanupEnemy();

		GameManager._Instance.MGameplay.CleanupTouch();

		for(int i = 0; i < num_enemies; ++i) 
		{
			int x = i % grid.Length;

			if(grid[x].Count > 0) 
			{
				int y = Random.Range(0, grid[x].Count);		
				Enemy e = (Instantiate(instanceEnemy[0]) as GameObject).GetComponent<Enemy>();
				e.SetPosition(grid[x][y].position);
                e.desiredZPosition = desiredZPositionForEnemies;
                Enemies.Add(e);
				grid[x].RemoveAt(y);
			}
		}

		SortEnemies();

        float biggest_delay = 0.0f;
    	for(int i = 0; i < num_enemies; ++i)
        {
    		GameManager._Instance.MGameplay.touchPositions.Add (
                    new Vector3 (   Enemies[i].GetPosition().x, 
                                    Enemies[i].GetPosition().y, 
                                    Enemies[i].desiredZPosition ) );

            float delay = 3.0f / (num_enemies - i) * 0.5f;
            if ( delay > biggest_delay ) biggest_delay = delay;
            Enemies[i].animationDelay = delay;
    	}

        Enemies[Enemies.Count - 1].SetCallback ( StartGameplay );
	}

    void StartGameplay ( )
    {
        Debug.Log ( "Started!" );
        GameManager._Instance.MGameplay.StartWave ( );
    }

	void SortEnemies() 
	{
		Enemies.Sort((c1, c2) => {
    		return c1.GetPosition().x.CompareTo(c2.GetPosition().x);
    	});
	}

	void cleanupEnemy() 
	{
		if(Enemies.Count > 0) 
		{
			foreach(Enemy e in Enemies) 
			{
				Destroy(e.gameObject);
			}
			Enemies.Clear();
		}
	}

	void createGrid(float gridSize) 
	{
		grid = new List<Grid>[4];

        for(int i = 0; i < 4; ++i)
        	grid[i] = new List<Grid>();

		// Left Top Grid
        for(float x = min_boundary.x; x < -gridSize / 2; x += gridSize) {
        	for(float y = min_boundary.y + max_boundary.y - gridSize; y < max_boundary.y; y += gridSize) {
        		grid[0].Add(new Grid(new Vector3(x, y, depth)));
        	}
        }

        // Right Top Grid
        for(float x = gridSize / 2; x < max_boundary.x; x += gridSize) {
        	for(float y = min_boundary.y + max_boundary.y - gridSize; y < max_boundary.y; y += gridSize) {
        		grid[1].Add(new Grid(new Vector3(x, y, depth)));
        	}
        }

        // Left Bottom Grid
        for(float x = min_boundary.x; x < -gridSize / 2; x += gridSize) {
        	for(float y = min_boundary.y - gridSize / 2; y < min_boundary.y + max_boundary.y / 2; y += gridSize) {
        		grid[2].Add(new Grid(new Vector3(x, y, depth)));
        	}
        }

        // Right Bottom Grid
        for(float x = gridSize / 2; x < max_boundary.x; x += gridSize) {
        	for(float y = min_boundary.y - gridSize / 2; y < min_boundary.y + max_boundary.y / 2; y += gridSize) {
        		grid[3].Add(new Grid(new Vector3(x, y, depth)));
        	}
        }
	}

    public void DestroyAllEnemies ( )
    {
        cleanupEnemy ( );
    }

    public void AllEnemiesFireAtPlayer ( )
    {
        foreach ( Enemy e in Enemies )
        {
            e.FireAtPlayer ( );
        }
    }

    public void AllEnemiesFireAtPlayer ( System.Action callback )
    {
        foreach ( Enemy e in Enemies )
        {
            e.FireAtPlayer ( callback );
        }
    }
}
