﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum GameplayState
{
    START = 0,
    UPDATE = 1,
    END = 2
}

public class Gameplay : MonoBehaviour
{
                        public      List<Vector3>   touchPositions          = new List<Vector3> ( );

    [HideInInspector]   public      int             currentLevel            = 0;
    [HideInInspector]   public      int             currentWave             = 0;
    [HideInInspector]   public      float           currentTime             = 0.0f;
    [HideInInspector]   public      float           timer                   = 0.0f;
    [HideInInspector]   public      int             playerLives             = 3;

    [HideInInspector]   public      bool            canTouch                = false;

                        private     bool            is_attacking            = false;
                        private     int             currentTouchIndex       = 0;
                        public      float           touchDistance           = 1.0f;
                        public      float           maxTouchDistance        = 10.0f;
                        public      float           timerTouch              = 0.5f;
                        private     float           timeWithNoTouch         = 0.0f;
                        private     Vector3         lastPoint               = Vector3.zero;

    [HideInInspector]   public      GameplayState   currentState            = GameplayState.START;

                        private     Vector3         targetTouchPos          = Vector3.zero;
                        private     float           currentTargetDistance   = 0.0f;
                        private     float           lastTargetDistance      = 0.0f;

    [SerializeField]    private     Text            timerCounter            = null;
    [SerializeField]    private     Image           timerImage              = null;
                        private     Coroutine       timerFunction           = null;

    [SerializeField]    private     Text            getReadyText            = null;
    [SerializeField]    private     Text            waveText                = null;
    [SerializeField]    private     Text            livesText               = null;
    [SerializeField]    private     Text            levelText               = null;

    [SerializeField]    private     GameObject      gameOverPanel           = null;

                        public      ParticleSystem  starsParticles          = null;

                        public      EnemyManager    enemyManager            = null;
                        public      Spaceship       playerSpaceship         = null;
                        public      Transform       playerTransform         = null;

                        public      DynamicLine     lineRenderer            = null;



    private void Awake ( )
    {
        if ( enemyManager == null )
            enemyManager = FindObjectOfType<EnemyManager> ( );
    }

    private void Start ( )
    {
        // initial setup
        Input.simulateMouseWithTouches = false;
        ResetUI ( );

        StartCoroutine ( StartGame ( ) );
    }

    private void Update ( )
    {
        if ( !canTouch ) return;

        if ( playerLives >= 0 )
        {
            switch ( currentState )
            {
                case GameplayState.START:

                    currentState = GameplayState.UPDATE;

                    break;

                case GameplayState.UPDATE:
                    
                    // update input
                    if ( (  Input.GetMouseButtonDown ( 0 ) ||
                            Input.touchCount > 0 ) && currentTouchIndex == 0 )
                    { 
                        GetNextTarget ( );
                    }
                    if ( Input.GetMouseButton ( 0 ) || 
                         Input.touchCount > 0 )
                    {
                        if ( currentTouchIndex > 0 )
                        {
                            timeWithNoTouch = 0.0f;
                            UpdateTouch ( Input.touchCount > 0 ? new Vector3 ( Input.touches [ 0 ].position.x, Input.touches [ 0 ].position.y, 0 ) : Input.mousePosition );
                        }
                    }
                    else
                    {
                        if ( currentTouchIndex > 0)
                        {
                            if ( timeWithNoTouch  >= timerTouch )
                            {
                                lineRenderer.OnHide ( );

                                canTouch = false;
                                PlayerLostLife ( );

                            }

                            timeWithNoTouch += GameManager._Instance.MGameTime;
                        }
                        
                    }
                    break;

                case GameplayState.END:

                    currentWave++;
                    if ( currentWave >= 5 )
                    {
                        currentLevel++;
                        playerLives = 3;
                        currentWave = 0;
                        levelText.text = "Level " + ( currentLevel + 1 ).ToString ( );
                    }

                    ResetUI ( );

                    StartCoroutine ( StartGame ( ) );
                    currentState = GameplayState.START;
                    canTouch = false;

                    enemyManager.DestroyAllEnemies ( );
                    
                    break;
            }
        }
    }

    public void CleanupTouch() 
    {
    	touchPositions.Clear();
    }

    private void UpdateTouch ( Vector2 touch_position )
    {
        Vector3 input = Camera.main.ScreenToWorldPoint ( new Vector3 ( touch_position.x, touch_position.y, 60 ) );

        if ( is_attacking )
        {
            currentTargetDistance = Vector3.Distance ( input, targetTouchPos );

            if ( currentTargetDistance - lastTargetDistance < 0f )
            {
                if ( Vector3.Distance ( input, targetTouchPos ) < touchDistance )
                {
                    // got there
                    GetNextTarget ( );
                }
            } 
            else
            {
                if (    Vector3.Distance ( input, targetTouchPos ) > maxTouchDistance &&
                        Vector3.Distance ( input, lastPoint ) > maxTouchDistance )
                {
                    // then there is something wrong here
                    //Debug.Log ( "FAILED" );
                }
            }

            lastTargetDistance = Vector3.Distance ( input, targetTouchPos );
            return;
        }
        
        is_attacking = Vector3.Distance ( input, targetTouchPos ) < touchDistance;
    }

    private void GetNextTarget (  )
    {
        if ( currentTouchIndex > 0 ) 
        {
            lineRenderer.addVectorPoint ( targetTouchPos );
            lineRenderer.OnDraw ( );
            lineRenderer.OnVisible ( );
        }

        if ( currentTouchIndex >= touchPositions.Count )
        {
            // win state
            playerSpaceship.FireAtEnemies ( touchPositions, OnWaveFinished );
            lineRenderer.can_continue = false;

            if ( timerFunction != null )
            {
                StopCoroutine ( timerFunction );
                timerFunction = null;
                canTouch = false;
            }

            return;
        }

        targetTouchPos = touchPositions [ currentTouchIndex++ ];
        
    }

    private void OnWaveFinished ( )
    {
        canTouch = true;
        currentState = GameplayState.END;

        lineRenderer.OnHide ( );
    }


    public void StartWave ( )
    {
        currentState = GameplayState.START;
        canTouch = true;
        lineRenderer.OnHide ( );
        lineRenderer.can_continue = true;
        lineRenderer.CleanupVector ( );
        currentTouchIndex = 0;
        timerFunction = StartCoroutine ( TimerCounter ( ) );
    }

    private void OnDrawGizmos()
    {
        for ( int i = 0; i < touchPositions.Count; i++ )
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere ( new Vector3 ( touchPositions[i].x, touchPositions[i].y, 0 ), touchDistance );
            if ( i < touchPositions.Count - 1 )
            Gizmos.DrawLine (   new Vector3 ( touchPositions[i].x, touchPositions[i].y, 10.0f ),
                                new Vector3 ( touchPositions[i + 1].x, touchPositions[i + 1].y, 10.0f ));
        }
    }

    private IEnumerator TimerCounter ( )
    {
        timerCounter.gameObject.SetActive ( true );
        timerImage.gameObject.SetActive ( true );

        currentTime = timer;

        do
        {
            currentTime -= Time.deltaTime;
            timerCounter.text = currentTime.ToString ( "#" ) + "s";
            timerImage.fillAmount = currentTime / timer;
            yield return null;
        } while ( currentTime >= 0.0f );
        
        timerCounter.text = 0 + "s";
        timerImage.fillAmount = 0;

        // player lost
        PlayerLostLife ( );
    }

    public void ResetUI ( )
    {
        lineRenderer.OnHide ( );

        timerCounter.gameObject.SetActive ( false );
        timerImage.gameObject.SetActive ( false );
        getReadyText.gameObject.SetActive ( false );
        waveText.gameObject.SetActive ( false );
        gameOverPanel.SetActive ( false );

        timerCounter.text = timer.ToString ( "#" ) + "s";
        timerImage.fillAmount = 1.0f;
    }

    public void StartGameWave ( )
    {
        int num_enemies = Mathf.Clamp( 2 + ( ( currentLevel * currentWave ) ) / 4 + ( currentWave % 2 ), 1, 6 );
        enemyManager.Respawn ( num_enemies );
        timer = 2.0f + ( num_enemies * 3.0f ) + ( 2.0f / Mathf.Max ( currentLevel, 1 ) );
    }

    private IEnumerator SpaceTravel ( )
    {
        getReadyText.gameObject.SetActive ( true );
        waveText.gameObject.SetActive ( true );
        
        waveText.text = "Wave " + ( currentWave + 1 ).ToString ( );

        float speed = 10.0f;
        var main = starsParticles.main;
        main.simulationSpeed = speed;
        yield return new WaitForSeconds ( 3.0f );
        
        getReadyText.gameObject.SetActive ( false );
        waveText.gameObject.SetActive ( false );

        do
        {
            speed = Mathf.Lerp ( speed, 2.0f, 2 * GameManager._Instance.MGameTime );
            main.simulationSpeed = speed;
            yield return null;
        } while ( speed > 2.2f );
        speed = 2.0f;
        main.simulationSpeed = speed;

        StartGameWave ( );
    }

    private IEnumerator StartGame ( )
    {
        yield return new WaitForSeconds ( 2.0f );
        StartCoroutine ( SpaceTravel ( ) );
    }

    private void PlayerLostLife ( )
    {
        canTouch = false;
        // play enemies fire animation
        enemyManager.AllEnemiesFireAtPlayer ( );

        StartCoroutine ( cooldown ( ) );
    }

    private IEnumerator cooldown ( )
    {
        yield return new WaitForSeconds ( 2.5f );
        AfterShot ( );
    }

    private void AfterShot ( )
    {
        if ( canTouch ) return;
        canTouch = true;
        playerLives--;
        if ( playerLives < 0 )
        {
            // player dead

            // game over
            gameOverPanel.SetActive ( true );
        }
        else
        {
            if ( timerFunction != null )
                StopCoroutine ( timerFunction );
            timerFunction = null;
            timerFunction = StartCoroutine ( TimerCounter ( ) ); 
            livesText.text = "Lives " + playerLives.ToString ( );
        }
        
        lineRenderer.OnHide ( );
        currentTouchIndex = 0;
    }

    public Vector3 GetPlayerPos ( )
    {
        return playerTransform.position;
    }

    public void TryAgain ( )
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene ( "Level" );
    }

    public void GoBackToMenu ( )
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene ( "Menu" );
    }
}
