﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicLine : MonoBehaviour {
	private LineRenderer lineRenderer;

	private List<Vector3> VectorPoints = new List<Vector3>( );
	private int indexTouchLineRenderer;

    public bool can_continue = true;


	public void Awake	( ) 
	{
        lineRenderer = GetComponent<LineRenderer>( );
	}

	public void FixedUpdate()
	{
		OnTouchLineRenderer( );
	}

	public void setVectorList( List<Vector3> VectorPoints )
	{
		this.VectorPoints = VectorPoints;
	}

	public void addVectorPoint( Vector3 Point )
	{
		VectorPoints.Add( Point );
	}

    public void CleanupVector()
    {
        VectorPoints.Clear ( );
    }

	public void OnHide( ) 
	{
		gameObject.SetActive ( false );
	}

	public void OnVisible( ) 
	{
		gameObject.SetActive ( true );
	}

	public void OnDraw( ) 
	{
        if ( lineRenderer == null )
            lineRenderer = GetComponent<LineRenderer>( );


		indexTouchLineRenderer = VectorPoints.Count;

		lineRenderer.positionCount = VectorPoints.Count + 1;
		Vector3 []points = new Vector3[lineRenderer.positionCount];

		int index = 0;

		// Enemies Position
		foreach( Vector3 pos in VectorPoints )
		{
            points[index] = pos;
			index++;
		}

		// Mouse Target Position
		points[index] = Camera.main.ScreenToWorldPoint( new Vector3( Input.mousePosition.x, Input.mousePosition.y, 60 ) );

		lineRenderer.SetPositions( points );
	}

	public void OnTouchLineRenderer( ) 	
	{
        if ( !can_continue )
        {
			lineRenderer.SetPosition( indexTouchLineRenderer, VectorPoints [ VectorPoints.Count - 1 ] );
            return;
        }
		if(lineRenderer.positionCount > 0) 
		{
			lineRenderer.SetPosition( indexTouchLineRenderer, Camera.main.ScreenToWorldPoint( new Vector3( Input.mousePosition.x, Input.mousePosition.y, 60 ) ) );
		}
	}
}
