﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

	private new Transform transform;
	private List<Vector3> Targets = new List<Vector3>();

    public Transform gunfireOrigin;
    public ParticleSystem m_fireFX;
    public ParticleSystem m_explosionFX;

    public AudioSource m_sound;

	void Start( ) 
	{
		transform = GetComponent<Transform>( );
	}

	public void addTarget( Vector3 target ) 
	{
		Targets.Add(target);
	}

	public void setTargetList( List<Vector3> targetList ) 
	{
		Targets = targetList;
	}

	public void OnPlay( )
	{
		StartCoroutine( MoveWeapon( ) );
	}

    public void OnPlay ( System.Action callback )
    {
		StartCoroutine ( MoveWeapon ( callback ) );
    }

	IEnumerator MoveWeapon( System.Action callback = null ) 
	{
        List<ParticleSystem> fx = new List<ParticleSystem> ( );
		foreach(Vector3 pos in Targets) {
			Vector3 RelativePos = pos - transform.position;
			Quaternion LookForward = Quaternion.LookRotation( RelativePos );
			do {
				transform.rotation = Quaternion.Lerp ( transform.rotation, LookForward, 15.0f * GameManager._Instance.MGameTime );
				yield return null;
			} while( !( Quaternion.Angle( transform.rotation, LookForward) < Mathf.Epsilon ) );

            var main = ( GameObject.Instantiate ( m_fireFX.gameObject, gunfireOrigin.position, Quaternion.identity ) as GameObject ).GetComponent<ParticleSystem> ( );
            main.Play ( );
            m_sound.Play ( );

            fx.Add ( main );

            // explosion animation
            main = Instantiate ( m_explosionFX, pos, Quaternion.identity );

            fx.Add ( main );
            
        }

        yield return new WaitForSeconds ( 1.2f );

        // clear up the FXs
        foreach ( ParticleSystem ps in fx )
            Destroy ( ps.gameObject );

        if ( callback != null )
            callback ( );

        do
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, 15.0f * GameManager._Instance.MGameTime);
            yield return null;
        } while (!(Quaternion.Angle(transform.rotation, Quaternion.identity) < Mathf.Epsilon));


        yield return new WaitForSeconds ( 0.5f );

        Targets.Clear( );
        fx.Clear ( );
	}
}
