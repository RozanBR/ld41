﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class EnemyData
{
	public float speed;

    public EnemyData ()
    { }
}


public class Enemy : MonoBehaviour {

	private new Transform transform = null;

	[SerializeField] private EnemyData data = new EnemyData ();
    public GameObject targetAnimation = null;
    [HideInInspector] public float desiredZPosition = 50.0f;

	public float animationDelay { get; set; }

    System.Action animationCallback = null;

    public Weapon enemyWeapon = null;
    
	private void Awake(  )
	{
		getComponents(  );
	}

	private void Start ( )
	{
		StartCoroutine ( EnemyRapidAnimation ( ) );
	}

	public void getComponents(  ) {
		transform = GetComponent<Transform>();
	}


	public void SetPosition ( Vector3 pos )
	{
		transform.position = pos;
	}

	public Vector3 GetPosition() 
	{
		if(transform == null) getComponents(  );
		return transform.position;
	}

	public void SetData ( EnemyData enemy_data )
	{
		data = enemy_data;
	}

    public void SetCallback ( System.Action callback )
    {
        animationCallback = callback;
    }

	IEnumerator EnemyRapidAnimation(  ) 
	{
		Vector3 endPos = transform.position; endPos.z = desiredZPosition;
		yield return new WaitForSeconds(animationDelay);

		// rapid-mod

		do
		{

			transform.position = 
				Vector3.Lerp(transform.position, endPos, 5.0f * GameManager._Instance.MGameTime);

			yield return null;

		} while ( Vector3.Distance ( transform.position, endPos ) > 1.0f );

		// slow-mod

		do
		{

			transform.position = 
				Vector3.Lerp(transform.position, endPos, 3.0f * GameManager._Instance.MGameTime);
			
			yield return null;

		} while ( Vector3.Distance ( transform.position, endPos ) > 0.1f );

        targetAnimation.SetActive ( true );

        if ( animationCallback != null ) animationCallback ( );
    }

    public void FireAtPlayer ( )
    {
        enemyWeapon.addTarget ( GameManager._Instance.MGameplay.GetPlayerPos ( ) );
        enemyWeapon.OnPlay ( );
    }

    public void FireAtPlayer ( System.Action callback )
    {
        enemyWeapon.addTarget ( GameManager._Instance.MGameplay.GetPlayerPos ( ) );
        enemyWeapon.OnPlay ( callback );
    }
}
