﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour
{
    private new Transform transform = null;


    public Weapon playerWeapon = null;

    private void Awake ( )
    {
        transform = GetComponent<Transform>();
    }



    public void FireAtEnemies ( List<Vector3> enemiesPositions )
    {
        playerWeapon.setTargetList ( enemiesPositions );
        playerWeapon.OnPlay ( );
    }

    public void FireAtEnemies ( List<Vector3> enemiesPositions, System.Action callback = null )
    {
        playerWeapon.setTargetList ( enemiesPositions );
        playerWeapon.OnPlay ( callback );
    }

}
