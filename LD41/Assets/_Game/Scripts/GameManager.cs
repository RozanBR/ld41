﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public enum GameState
{
    RUNNING = 1,
    PAUSED = 0
}

public delegate void OnGameStateChanged ( );

public class GameManager : MonoBehaviour
{

    public static OnGameStateChanged OnGamePaused;
    public static OnGameStateChanged OnGameResumed;

    static private GameManager _instance = null;
    static public GameManager _Instance
    {
        get
        {
            if ( _instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
                if ( _instance == null )
                {
                    Debug.LogError ( "There's no Game Manager Available!!" );
                }
            }

            return _instance;
        }
    }
    
    public float MGameTime { private set; get; }
    public GameState MGameState { private set; get; }

    public Gameplay MGameplay { private set; get; }


    private void OnEnable()
    {
        SceneManager.sceneLoaded += scene_loaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= scene_loaded;
    }

    private void Awake()
    {
        if ( _instance != null && _instance != this )
        {
            Destroy ( gameObject );
            return;
        } 
        else
        {
            _instance = this;
            DontDestroyOnLoad ( gameObject );
        }
    }

    private void Start ( )
    {
        ResumeGame ( );
    }


    #region Game State Functions

    public void PauseGame ( )
    {
        MGameState = GameState.PAUSED;
        update_game_time ( );

        if ( OnGamePaused != null ) OnGamePaused ( );
    }

    public void ResumeGame ( )
    {
        MGameState = GameState.RUNNING;
        update_game_time ( );

        if ( OnGameResumed != null ) OnGameResumed ( );
    }

    private void update_game_time ( )
    {
        MGameTime = ( MGameState == GameState.RUNNING ) ? Time.fixedDeltaTime : 0.0f;
    }

    #endregion


    #region Load Scene

    private void scene_loaded ( Scene scene, LoadSceneMode mode )
    {
        MGameplay = FindObjectOfType<Gameplay>();
    }

    #endregion

}
